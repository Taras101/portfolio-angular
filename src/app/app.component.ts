import { Component } from '@angular/core';
import { Lightbox } from 'angular2-lightbox';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Taras Petryk';
  private _albums: Array<any> = [];

  constructor(private _lightbox: Lightbox) {
    const s3 = 'https://s3-us-west-2.amazonaws.com/s1portfolio/images/2018/'
    this._albums = [
        {src : s3 + 'Grove.png', caption: 'Grove', thumb : s3 + 'grove-thumb.png',url: 'http://caseysokol.com/simple_grove/home.html'},
         {src : s3 + 'MJR-vr.png', caption: 'My Job Rocks VR', thumb : s3 + 'MJR-vr-thumb.png',url: 'http://myjobrocks.ca/vr/'},
         {src : s3 + 'CC.png', caption: 'Crafty Containers', thumb : s3 + 'CC-thumb.png',url: 'http://craftycontainers.ca'},
         {src : s3 + 'FL.png', caption: 'Future Legends', thumb : s3 + 'FL-thumb.png',url: 'http://futurelegendstv.ca/'},
         {src : s3 + 'FL-vr.png', caption: 'Future Legends VR', thumb : s3 + 'FL-vr-thumb.png',url: 'http://futurelegendstv.ca/vr/#/'},
         {src : s3 + 'UMS.png', caption: 'The Underground Music Scene', thumb : s3 + 'UMS-thumb.png',url: 'http://www.theundergroundmusicscene.ca/'},
         {src : s3 + 'mjr-ionic.png', caption: 'My Job Rocks App', thumb : s3 + 'mjr-ionic-thumb.png',url: 'https://itunes.apple.com/us/developer/rvk-productions/id1020421356?mt=8'},
         {src : s3 + 'MJR-arcade.png', caption: 'My Job Rocks Arcade', thumb : s3 + 'MJR-arcade-thumb.png',url: 'http://myjobrocks.ca/mountain/#/arcade'}
      ]
    // for (let i = 0; i <= 2; i++) {
    // const src = album1[i].src;
    // const caption = album1[i].caption;
    // const thumb = album1[i].thumb;

    //      const album = {
    //      src: src,
    //      caption: caption,
    //      thumb: thumb
    //   };



  }
  ngOnInit(){
    AFRAME.registerComponent('cursor-listener', {
      init: function () {
        this.el.addEventListener('click', function (evt) {
          this.setAttribute('material', 'visible', false);
          this.setAttribute('position', {x: 0, y: 10, z: 0});
        });
      }
    });
    AFRAME.registerComponent('cursor-listener1', {
      init: function () {
        this.el.addEventListener('click', function (evt) {
          var cam = document.getElementById('camHolder');

          setTimeout(function(){
          cam.setAttribute('rotation', '0 0 0');
          }, 1500);
        });
      }
    });
    document.querySelector('a-scene').addEventListener('enter-vr', function () {
       console.log("ENTERED VR");
    });

     function screenSwitch(){
      var div = document.getElementById('virtual');
        if (div.style.display !== 'none') {
            div.style.display = 'none';
        }
        else {
            div.style.display = 'block';
        }

     }
  }
  scrollTop = () => window.scrollTo(0, 0);

  open(index: number): void {
    // open lightbox
    console.log("lightbox");
    this._lightbox.open(this._albums, index);
  }

}



